package at.datenwort.idea.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

@Aspect
public class MyAspect {
    @Around("execution(@at.datenwort.idea.aspect.MyAspectAnnotation * *(..))")
    public Object executeTransactional(final ProceedingJoinPoint joinPoint) throws Throwable {
        long ret = (long) joinPoint.proceed();
        return ret + 1;
    }
}
