package at.datenwort.idea;

import at.datenwort.idea.aspect.MyAspectAnnotation;

public class App {
    @MyAspectAnnotation
    public long theInterceptedMethod() {
        return 99;
    }

    public static void main(String[] args) {
        System.out.println("Should print 100: " + new App().theInterceptedMethod());
    }
}
