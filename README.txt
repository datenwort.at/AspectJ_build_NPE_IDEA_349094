Sample project to reproduce the NPE when "Post-compile weave mode is enabled".

Therefore, don't forget to switch to AJC compiler and enable that flag in the module settings.

If you den "Recompile all", you will get that

Module 'AspectJ_build_NPE_IDEA_349094' production: java.lang.NullPointerException: Cannot read the array length because "buf" is null

exception.
